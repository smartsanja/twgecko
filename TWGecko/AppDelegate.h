//
//  AppDelegate.h
//  TWGecko
//
//  Created by Sanj on 9/27/15.
//  Copyright (c) 2015 appgeeks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

