//
//  SettingsViewController.m
//  TWGecko
//
//  Created by Sanj on 9/27/15.
//  Copyright (c) 2015 appgeeks. All rights reserved.
//

#import "SettingsViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "Reachability.h"
#import "Config.h"
#import "LLARingSpinnerView.h"
#import "AsyncImageView.h"

@interface SettingsViewController ()
@property (nonatomic, strong) LLARingSpinnerView *spinnerView;
@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // UI Configurations
    self.title = @"Sign in with Twitter";
    // coloring
    self.navigationController.navigationBar.barTintColor = navigationBarColor;
    self.navigationController.navigationBar.tintColor = navigationBarTextColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   navigationBarTextColor, NSForegroundColorAttributeName,
                                                                   [UIFont systemFontOfSize:17], NSFontAttributeName, nil];
    
    // back button
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"backBtn"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(btnBackTapped)];
    
    self.navigationItem.leftBarButtonItem = backBtn;
    
    
    //loading spinner
    self.spinnerView = [[LLARingSpinnerView alloc] initWithFrame:CGRectZero];
    self.spinnerView.bounds = CGRectMake(0, 0, 40, 40);
    self.spinnerView.tintColor = navigationBarColor;
    self.spinnerView.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds));
    [self.view addSubview:self.spinnerView];
    [self.spinnerView setHidden:YES];
    
    
    // check for internet connection
    if (![self connected]) {
        NSLog(@"No Internet");
        [self displayErrorMsg:KNoInternetMsg];
        
    } else {
        NSLog(@"Internet Available");
        
        // Check the twitter session is available
        TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
        TWTRSession *lastSession = store.session;
        
        if(lastSession == NULL){
            // No active twitter session
            [self.imgViewProfilePic setHidden:YES];
            [self.lblLoggedinStatus setHidden:YES];
            [self.lblUserName setHidden:YES];
            [self.lblFullName setHidden:YES];
            [self.btnLogout setHidden:YES];
            
            [self.btnLogin setHidden:NO];
        }
        else{
            // There's an active twitter session
            // display current user details
            [[[Twitter sharedInstance] APIClient] loadUserWithID:[lastSession userID] completion:^(TWTRUser *user,NSError *error){
                
                if(user){
                    
                    // No active twitter session
                    [self.imgViewProfilePic setHidden:NO];
                    [self.lblLoggedinStatus setHidden:NO];
                    [self.lblUserName setHidden:NO];
                    [self.lblFullName setHidden:NO];
                    [self.btnLogout setHidden:NO];
                    
                    [self.btnLogin setHidden:YES];
                    
                    
                    if(user.name != nil){
                        // get full name
                        NSArray *twonames = [user.name componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                        
                        NSString *strFirstName = [twonames objectAtIndex:0];
                        NSString *strLastName = [twonames lastObject];
                        
                        // display users full name
                        self.lblFullName.text = [NSString stringWithFormat:@"%@ %@", strFirstName, strLastName];
                        // twitter name
                        self.lblUserName.text = [lastSession userName];
                        
                    }
                    
                    // download twitter profile image
                    [self performSelector:@selector(dowloadTwitterProfilePic:) withObject:user.profileImageLargeURL];
                    
                }
                else{
                    NSLog(@"error: %@", [error localizedDescription]);
                    [self displayErrorMsg:[error localizedDescription]];
                }
                
            }];
           
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// check for an active internet connection
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


#pragma mark - Action methods

- (void)btnBackTapped{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnLoginTapped:(id)sender {
    
    // show in the status bar that network activity is starting
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    // show loading indicater
    [self.spinnerView startAnimating];
    [self.spinnerView setHidden:NO];
    
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            
            NSLog(@"signed in as %@", [session userName]);
            
            [[[Twitter sharedInstance] APIClient] loadUserWithID:[session userID]
                                                      completion:^(TWTRUser *user,
                                                                   NSError *error){
                 if(user){
                     
                     // No active twitter session
                     [self.imgViewProfilePic setHidden:NO];
                     [self.lblLoggedinStatus setHidden:NO];
                     [self.lblUserName setHidden:NO];
                     [self.lblFullName setHidden:NO];
                     [self.btnLogout setHidden:NO];
                     
                     [self.btnLogin setHidden:YES];
                    
                     
                    if(user.name != nil){
                        // get full name
                        NSArray *twonames = [user.name componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                        
                        NSString *strFirstName = [twonames objectAtIndex:0];
                        NSString *strLastName = [twonames lastObject];
                        
                        // display users full name
                        self.lblFullName.text = [NSString stringWithFormat:@"%@ %@", strFirstName, strLastName];
                        // twitter name
                        self.lblUserName.text = [session userName];
                        
                     }
                     
                     // download twitter profile image
                     [self performSelector:@selector(dowloadTwitterProfilePic:) withObject:user.profileImageLargeURL];
                     
                     
                     // send user logedin notification to TimelineViewController
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"UserLoggedIn" object:self userInfo:nil];
                     
                }
                else{
                    NSLog(@"error: %@", [error localizedDescription]);
                    [self displayErrorMsg:[error localizedDescription]];
                }
                 
             }];
            
        } else {
            
            NSLog(@"error: %@", [error localizedDescription]);
            // display error message
            [self displayErrorMsg:[error localizedDescription]];
        }
    }];
    
}

- (void) dowloadTwitterProfilePic :(NSString *) twitterImageUrl{
    
    
    if (self.imgViewProfilePic.image != nil)
    {
        //cancel loading previous image
        [[AsyncImageLoader sharedLoader] cancelLoadingImagesForTarget:self.imgViewProfilePic];
    }
    
    //load the profile image
    self.imgViewProfilePic.imageURL = [NSURL URLWithString:twitterImageUrl];
    
    // stop loading spinner
    [self.spinnerView stopAnimating];
    [self.spinnerView setHidden:YES];
    
}


- (IBAction)btnLogoutTapped:(id)sender {
    
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    NSString *userID = store.session.userID;
    
    [store logOutUserID:userID];
    
    [self.imgViewProfilePic setHidden:YES];
    [self.lblLoggedinStatus setHidden:YES];
    [self.lblUserName setHidden:YES];
    [self.lblFullName setHidden:YES];
    [self.btnLogout setHidden:YES];
    
    [self.btnLogin setHidden:NO];
    
    // send user loged out notification to TimelineViewController
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserLoggedOut" object:self userInfo:nil];
}

#pragma mark -
#pragma mark - Error handling

- (void)displayErrorMsg:(NSString *)errorMsg
{
    UIAlertView *altError = [[UIAlertView alloc]initWithTitle:@"Something went wrong!" message:errorMsg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [altError show];
}

@end
