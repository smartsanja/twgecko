//
//  SettingsViewController.h
//  TWGecko
//
//  Created by Sanj on 9/27/15.
//  Copyright (c) 2015 appgeeks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblLoggedinStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;


- (IBAction)btnLogoutTapped:(id)sender;
- (IBAction)btnLoginTapped:(id)sender;


@end
