//
//  TimelineViewController.m
//  TWGecko
//
//  Created by Sanj on 9/27/15.
//  Copyright (c) 2015 appgeeks. All rights reserved.
//

#import "TimelineViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "SettingsViewController.h"
#import "LLARingSpinnerView.h"
#import "Reachability.h"
#import "Config.h"

@interface TimelineViewController ()
@property (nonatomic, strong) LLARingSpinnerView *spinnerView;
@end

@implementation TimelineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // NSNotification Observers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLoggedInNotice:) name:@"UserLoggedIn" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLoggedOutNotice:) name:@"UserLoggedOut" object:nil];
    
    // UI Configurations
    self.title = @"Gecko Twitter Client";
    
    // coloring
    self.view.backgroundColor = navigationBarTextColor;
    self.navigationController.navigationBar.barTintColor = navigationBarColor;
    self.navigationController.navigationBar.tintColor = navigationBarTextColor;
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   navigationBarTextColor, NSForegroundColorAttributeName,
                                                                   [UIFont systemFontOfSize:17], NSFontAttributeName, nil];
    
    // settings button
    UIBarButtonItem *settingsBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"settingsBtn"]
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(settingsButtonTapped)];
    
    self.navigationItem.leftBarButtonItem = settingsBtn;
    
    // compose button
    UIBarButtonItem *composeBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"tweetBtn"]
                                                                 style:UIBarButtonItemStylePlain
                                                                target:self
                                                                action:@selector(composeButtonTapped)];
    
    self.navigationItem.rightBarButtonItem = composeBtn;
    
    
    // check for internet connection
    if (![self connected]) {
        NSLog(@"No Internet");
        [self displayErrorMsg:KNoInternetMsg];
        
    }
    else {
        NSLog(@"Internet Available");
        
        // Check the twitter session is available
        TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
        TWTRSession *lastSession = store.session;
        
        if(lastSession == NULL){
            // No active twitter session, we'll show the login window
            [self settingsButtonTapped];
        }
        else{
            // load current user's timeline
            [self loadUserTimeLine];
        }
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// check for an active internet connection
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}


#pragma mark - Action methods

-(void)loadUserTimeLine{
    
    // get the current session
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    TWTRSession *lastSession = store.session;
    
    // load current user's time line
    TWTRAPIClient *client = [[TWTRAPIClient alloc] init];
    TWTRUserTimelineDataSource *userTimelineDataSource = [[TWTRUserTimelineDataSource alloc] initWithScreenName:[lastSession userName] APIClient:client];
    // Show Tweet actions
    self.showTweetActions = true;
    
    self.dataSource = userTimelineDataSource;
    
    NSLog(@"DataSource %@", self.dataSource);
    [self reloadInputViews];
    
}

-(void)settingsButtonTapped{
    
    SettingsViewController *obj = [[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:obj];
    [self presentViewController:nav animated:NO completion:nil];
}

-(void)composeButtonTapped{
    
    // Check the twitter session is available
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    TWTRSession *lastSession = store.session;
    
    if(lastSession == NULL){
        // No active twitter session, we'll show a message
        UIAlertView *altLogIn = [[UIAlertView alloc]initWithTitle:@"Login Required" message:@"Please logged in to your twitter account in the Settings page" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [altLogIn show];
    }
    else{
        // open the composer
        TWTRComposer *composer = [[TWTRComposer alloc] init];
        [composer setText:@"Hello TradeGecko!"];
        [composer setImage:[UIImage imageNamed:@"tradeGeckoLogo"]];
        
        // Called from a UIViewController
        [composer showFromViewController:self completion:^(TWTRComposerResult result) {
            if (result == TWTRComposerResultCancelled) {
                NSLog(@"Tweet composition cancelled");
            }
            else {
                NSLog(@"Sending Tweet!");
            }
        }];
    }
    
    
}

#pragma mark -
#pragma mark - Notification Reciever methods
- (void)getLoggedInNotice:(NSNotification *)notify{

    NSLog(@"User logged in");
    
    // re-load user time line
    [self loadUserTimeLine];
}

- (void)getLoggedOutNotice:(NSNotification *)notify{
    
    NSLog(@"User logged out");
    
    // reset time line datasource
    self.dataSource = nil;
    [self reloadInputViews];
    
}

    
#pragma mark -
#pragma mark - Error handling

- (void)displayErrorMsg:(NSString *)errorMsg
{
    UIAlertView *altError = [[UIAlertView alloc]initWithTitle:@"Something went wrong!" message:errorMsg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [altError show];
}


@end
