//
//  TimelineViewController.h
//  TWGecko
//
//  Created by Sanj on 9/27/15.
//  Copyright (c) 2015 appgeeks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TwitterKit/TwitterKit.h>

@interface TimelineViewController : TWTRTimelineViewController

@end
